import { Component, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { CampaignsService } from '../../service/campaigns.service';
import { ICampaigns } from 'mook-data/campaigns';

@Component({
  selector: 'app-campaigns-table',
  templateUrl: './campaigns-table.component.html',
  styleUrls: ['./campaigns-table.component.scss']
})
export class CampaignsTableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = [ 'CSID', 'customer', 'name', 'startDate', 'endDate', 'status'];
  dataSource: MatTableDataSource<ICampaigns>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor( private campaignService: CampaignsService ) {}

  ngOnInit() {
    this.campaignService
      .getCampaigns()
      .subscribe( data => {
        this.dataSource = new MatTableDataSource(data);
    });
  }


  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  public isDate( key: string): boolean {
    return key.includes('Date');
  }

  public parseDate(date: number): number {
    return date * 1000;
  }

  public parseKey(key: string): string {
    switch (key) {
      case 'CSID': return 'CS-ID';
      case 'name': return 'Kampagnenname';
      case 'customer': return 'Kunde';
      case 'startDate': return 'Start';
      case 'endDate': return 'Ende';
      case 'status': return 'Status';
      default: return key;
    }
  }
}
