import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { eStatus } from 'mook-data/campaigns';
import { MatSelectChange } from '@angular/material/select';
import { CampaignsService } from 'src/app/service/campaigns.service';

@Component({
  selector: 'app-filter-campaigns',
  templateUrl: './filter-campaigns.component.html',
  styleUrls: ['./filter-campaigns.component.scss']
})
export class FilterCampaignsComponent {

  status = new FormControl();
  statusList: string[] = [eStatus.Archived, eStatus.Booked, eStatus.Created];

  constructor(private campaignService: CampaignsService) {}

  onChange(event: MatSelectChange) {
    this.campaignService.filterCampaigns(event.value);
  }

  public removeFilter(event: MouseEvent, filter: string = '') {
    event.stopPropagation();
    const newFilters: string[] = filter ?
      this.status.value.filter( oldFilter => oldFilter !== filter ) :
      [];
    this.status.setValue(newFilters);
    this.campaignService.filterCampaigns(newFilters);
  }

}
