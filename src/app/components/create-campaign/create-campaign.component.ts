import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { eStatus, ICampaigns } from 'mook-data/campaigns';
import { CampaignsService } from 'src/app/service/campaigns.service';

@Component({
  selector: 'app-create-campaign',
  templateUrl: './create-campaign.component.html',
  styleUrls: ['./create-campaign.component.scss']
})
export class CreateCampaignComponent{

  campaignForm = new FormGroup({
    name: new FormControl('', [ Validators.required ]),
    customer: new FormControl('', [ Validators.required ]),
    startDate: new FormControl(new Date()),
    endDate: new FormControl(new Date()),
    status: new FormControl(eStatus.Created),
  });
  statusList: string[] = [eStatus.Archived, eStatus.Booked, eStatus.Created];

  constructor(
    public dialogRef: MatDialogRef<CreateCampaignComponent>,
    private campaignService: CampaignsService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  addCampaign() {
    console.log(this.campaignForm);

    if ( this.campaignForm.invalid ) {
      console.log('error on form');
      return;
    }

    const newCampaign: ICampaigns = {
      CSID: this.campaignService.getTotal().value.length + 1,
      name: this.campaignForm.get('name').value,
      customer: this.campaignForm.get('customer').value,
      startDate: this.getDate('startDate'),
      endDate: this.getDate('endDate'),
      status: this.getStatus(),
    };
    this.campaignService.addCampaign(newCampaign);
    this.dialogRef.close();
  }

  getDate(key: string): number {
    return new Date( this.campaignForm.get(key).value ).getTime() / 1000;
  }

  getStatus(): eStatus {
    return this.campaignForm.get('status').value;
  }

  public errorLabel(key: string): boolean {
    return !this.campaignForm.get(key).untouched && (
      this.campaignForm.get(key).hasError('required') ||
      this.campaignForm.get(key).hasError('invalid')
    );
  }
}
