import { Injectable } from '@angular/core';
import { CAMPAIGNS, ICampaigns, eStatus } from 'mook-data/campaigns';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignsService {

  private fullData: ICampaigns[] = CAMPAIGNS;
  private totalData: BehaviorSubject<ICampaigns[]> = new BehaviorSubject<ICampaigns[]>([]);
  private displayData: BehaviorSubject<ICampaigns[]> = new BehaviorSubject<ICampaigns[]>([]);
  private currentFilters: eStatus[] = [];

  constructor() {
    this.displayData.next(this.fullData);
    this.totalData.next(this.fullData);
  }

  public getTotal(): BehaviorSubject<ICampaigns[]> {
    return this.totalData;
  }

  public getCampaigns(): BehaviorSubject<ICampaigns[]> {
    return this.displayData;
  }

  public filterCampaigns( status: string[] ): void {
    this.currentFilters = status.map( s => s as eStatus );
    const filtered: ICampaigns[] = this.fullData
      .filter( campaign => !status.length || status.includes(campaign.status) );
    this.displayData.next(filtered);
  }

  public addCampaign(newCampaign: ICampaigns) {
    this.fullData.push(newCampaign);
    this.totalData.next(this.fullData);

    if ( this.currentFilters.includes(newCampaign.status) ) {
      const filtered = this.displayData.value;
      filtered.push(newCampaign);
      this.displayData.next(filtered);
    }

  }
}
