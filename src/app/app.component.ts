import { Component, OnInit } from '@angular/core';
import { CampaignsService } from './service/campaigns.service';
import {MatDialog} from '@angular/material/dialog';
import { CreateCampaignComponent } from './components/create-campaign/create-campaign.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  displaying: number;
  total: number;

  constructor( private campaignService: CampaignsService, public dialog: MatDialog ) { }

  ngOnInit() {
    this.campaignService.getTotal().subscribe( total => this.total = total.length );
    this.campaignService.getCampaigns().subscribe( displayed => this.displaying = displayed.length );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateCampaignComponent, {width: '970px'});

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed and sent: ' + result);
    });
  }
}
