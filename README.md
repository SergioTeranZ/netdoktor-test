# Netdoktor Test

Netdoktor Assignment - Frontend

## The Assignment

Campaign manager is a web app to create and manage ad campaigns. In this assignment
we would like you to create an overview screen and a create campaign popup. The required
features are:

* An overview with all existing campaigns
  * The list with the campaigns can be filtered based on status, multiple statuses can be selected
* Create a new campaign
  * The campaign should have the following fields: 
    * Campaign Name
    * Customer
    * Start and End date
    * Status
  * The Campaign Name and Customer fields are **required**
  * The Status field can get one of the predefined options: 
    * Created
    * Booked
    * Archived
  * The campaign should get a unique ID (CS-ID) upon creation